//
//  Character.swift
//  BreakingBad
//
//  Created by Admin on 20.03.21.
//

import Foundation
struct Character: Decodable{
    var name: String
    var birthday: String
    var img: String
    var appearance: [Int]?
    
    enum CodingKeys: String, CodingKey{
        case name
        case birthday
        case img
        case appearance
    }
    
}
