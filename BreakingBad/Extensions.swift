//
//  Extensions.swift
//  BreakingBad
//
//  Created by Admin on 20.03.21.
//

import Foundation
import UIKit
import Kingfisher

extension UIImageView{
    func imageFrom(url:URL){
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url){
                if let image = UIImage(data:data){
                    DispatchQueue.main.async{
                        self?.image = image
                    }
                }
            }
        }
    }
}

extension UIImageView{
    func loadImage(url:URL, resize:Bool){
        
        let resizingProcessorresizingProcessor = ResizingImageProcessor(referenceSize: CGSize(width: 350 , height: 350 ))
        
        let processor = resize ? resizingProcessorresizingProcessor : DownsamplingImageProcessor(size: CGSize(width: 350 , height: 350 ))
            |> RoundCornerImageProcessor(cornerRadius: 10)

        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
    }
}

//
//extension UISearchBarDelegate {
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String, characters: Array<Character>) -> Array<Character> {
//        var searchedCharacters = characters.filter { $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() }
//        //isSearching = true
//        //self.tableView.reloadData()
//    
//    return searchedCharacters
//    }
//    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//            //isSearching = false
//            searchBar.text = ""
//            //tableView.reloadData()
//        }
//}
//


