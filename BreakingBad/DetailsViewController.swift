//
//  ViewController.swift
//  BreakingBad
//
//  Created by Admin on 17.03.21.
//

import UIKit
import Kingfisher

class DetailsViewController: UIViewController {

    var character: Character?
    
    @IBOutlet weak var characterImage: UIImageView!
    @IBOutlet weak var characterName: UILabel!
    @IBOutlet weak var characterAppearance: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = character?.img ?? ""
        characterImage.loadImage(url: URL(string: image)!, resize: false)
        characterName.text=character?.name
        
        let appearance = character?.appearance?.description ?? ""
        characterAppearance.text="Appearance \(appearance)"
    }
    
}

