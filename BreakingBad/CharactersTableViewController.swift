//
//  CharactersTableView.swift
//  BreakingBad
//
//  Created by Admin on 17.03.21.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class CharactersTableViewController: UITableViewController{
    
    @IBOutlet var charactersTableView: UITableView!
    @IBOutlet weak var charactersSearchBar: UISearchBar!
    @IBOutlet weak var seasonButton: UIBarButtonItem!
    
    var characters = [Character]()
    var searchedCharacters = [Character]()
    var isSearching = false
    
    var seasonPickerView: UIPickerView!
    var seasonPickerData: [Int]!
    
    @IBAction func seasonButtonClick(_ sender: Any) {
        showNumberPicker()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (isSearching==true) ? searchedCharacters.count : characters.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _ = UIStoryboard(name:"Main",bundle:nil);
        performSegue(withIdentifier: "showdetail", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? DetailsViewController {
            let index = charactersTableView.indexPathForSelectedRow?.row ?? 0
            let selectedCharacter = (isSearching==true) ? searchedCharacters[index] :
                characters[index]
            destination.character = selectedCharacter
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "UserTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? UserTableViewCell else {
            fatalError("The dequeued is not an instance of UserViewCell")
        }
        
        let character = (isSearching==true) ? searchedCharacters[indexPath.row] : characters[indexPath.row]
        
        cell.characterName.text = character.name
        cell.characterImage.loadImage(url: URL(string: character.img)!, resize: true)
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func viewDidLoad() {
        callBreakingBadApi()
        
        self.charactersSearchBar.delegate = self
        
        seasonPickerView = UIPickerView(frame: CGRect(x:10,y:50, width:250, height: 150))
        seasonPickerView.delegate = self
        seasonPickerView.dataSource = self
        
        let minNum = 1
        let maxNum = 10
        seasonPickerData = Array(stride(from: minNum, to: maxNum+1, by: 1))
    }
    
    func callBreakingBadApi(){
        AF.request("https://breakingbadapi.com/api/characters").validate(statusCode: 200..<300)
            .responseDecodable(of: [Character].self){
                (response) in
                guard let charactersResponse = response.value else {return}
                
                for character in charactersResponse {
                    print(character.name)
                }
                
                self.characters = charactersResponse
                
                self.characters=charactersResponse
                print(self.characters.count)
                
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
    }
    
    
}

extension CharactersTableViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchedCharacters = characters.filter { $0.name.lowercased().prefix(searchText.count) == searchText.lowercased() }
        isSearching = true
        self.tableView.reloadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
        searchBar.text = ""
        tableView.reloadData()
    }
    
}

extension CharactersTableViewController: UIPickerViewDelegate {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(seasonPickerData[row])"
    }
    
    func showNumberPicker(){
        let ac = UIAlertController(title: "Picker", message: "\n\n\n\n\n\n\n\n\n\n", preferredStyle: .alert)
        ac.view.addSubview(seasonPickerView)
        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            let pickerValue = self.seasonPickerData[self.seasonPickerView.selectedRow(inComponent: 0)]
            print("Picker value: \(pickerValue) was selected")
            
            self.searchedCharacters = self.characters.filter {
                $0.appearance != nil && $0.appearance!.count>0 && $0.appearance!.contains(pickerValue)
            }
            self.isSearching = true
            self.tableView.reloadData()
        }))
        ac.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(ac, animated: true)
    }
    
}

extension CharactersTableViewController: UIPickerViewDataSource {
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return seasonPickerData.count
    }
}
