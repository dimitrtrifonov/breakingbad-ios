# BreakingBad iOS Application

BreakingBad-iOS is an iOS Application that fetches API endpoint https://breakingbadapi.com/api/characters
and displays data in a list. Character details are displayed on the second screen.

## Architecture
The application is created using Alamofire and SwiftyJSON.